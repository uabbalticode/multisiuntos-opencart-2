<?php
require_once(DIR_SYSTEM . 'library/weight.php');
require_once(DIR_APPLICATION . 'model/multisiuntos/createdownload.php');
class ControllerModuleMultisiuntos extends Controller {
	private $express_available = array(
        'TNT',
        'TNT_LT',
        'TNT_LV',
        'TNT_EE',
        'DHL',
        'DHL_LT',
        'DHL_LV',
        'DHL_EE',
        'UPS_LT',
        'UPS_LV',
        'UPS_EE',
    );
    private $cod_methods = array('cod');
	private $error = array();
	private $show_courier_list = array(
		array('courier_id' => 'OMNIVA_LT', 'name' => 'OMNIVA_LT'),
		array('courier_id' => 'OMNIVA_LV', 'name' => 'OMNIVA_LV'),
		array('courier_id' => 'OMNIVA_EE', 'name' => 'OMNIVA_EE'),
		array('courier_id' => 'LP_EXPRESS', 'name' => 'LP_EXPRESS'),
		array('courier_id' => 'VENIPAK', 'name' => 'VENIPAK'),
		array('courier_id' => 'DPD_LT', 'name' => 'DPD_LT'),
		array('courier_id' => 'DPD_LV', 'name' => 'DPD_LV'),
		array('courier_id' => 'DPD_EE', 'name' => 'DPD_EE'),
		array('courier_id' => 'UPS', 'name' => 'UPS'),
		array('courier_id' => 'UPS_LT', 'name' => 'UPS_LT'),
		array('courier_id' => 'UPS_LV', 'name' => 'UPS_LV'),
		array('courier_id' => 'UPS_EE', 'name' => 'UPS_EE'),
		array('courier_id' => 'TNT', 'name' => 'TNT'),
		array('courier_id' => 'TNT_LT', 'name' => 'TNT_LT'),
		array('courier_id' => 'TNT_LV', 'name' => 'TNT_LV'),
		array('courier_id' => 'TNT_EE', 'name' => 'TNT_EE'),
		array('courier_id' => 'DHL', 'name' => 'DHL'),
		array('courier_id' => 'DHL_LT', 'name' => 'DHL_LT'),
		array('courier_id' => 'DHL_LV', 'name' => 'DHL_LV'),
		array('courier_id' => 'DHL_EE', 'name' => 'DHL_EE'),
		array('courier_id' => 'LT_POST', 'name' => 'LT_POST'),
		array('courier_id' => 'LV_POST', 'name' => 'LV_POST'),
		array('courier_id' => 'ITELLA', 'name' => 'ITELLA'),
		array('courier_id' => 'SMARTBUS', 'name' => 'SMARTBUS'),
	);
	private $shipping_list = array (
								['module' => 'auspost', 'method' => 'auspost.standard', 'title' => 'Australia Post Standard', 'default' => null],
								['module' => 'auspost', 'method' => 'auspost.express', 'title' => 'Australia Post Express', 'default' => null],
								['module' => 'citylink', 'method' => 'citylink.citylink', 'title' => 'Citylink', 'default' => null],
								['module' => 'fedex', 'method' => 'fedex.', 'title' => '', 'default' => null],
								['module' => 'flat', 'method' => 'flat.flat', 'title' => 'Flat Shipping Rate', 'default' => null],
								['module' => 'free', 'method' => 'free.free', 'title' => 'Free Shipping', 'default' => null],
								['module' => 'item', 'method' => 'item.item', 'title' => 'Per Item Shipping Rate', 'default' => null],
								['module' => 'parcelforce_48', 'method' => 'parcelforce_48.parcelforce_48', 'title' => 'Parcelforce 48', 'default' => null],
								['module' => 'pickup', 'method' => 'pickup.pickup', 'title' => 'Pickup', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.1st_class_standard', 'title' => 'Royal Mail First Class Standard Post', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.1st_class_recorded', 'title' => 'Royal Mail First Class Recorded Post', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.special_delivery_500', 'title' => 'Royal Mail Special Delivery Next Day (£500)', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.special_delivery_1000', 'title' => 'Royal Mail Special Delivery Next Day (£1000)', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.special_delivery_2500', 'title' => 'Royal Mail Special Delivery Next Day (£2500)', 'default' => null],
								['module' => 'royal_mail', 'method' => 'royal_mail.standard_parcels', 'title' => 'Royal Mail Standard Parcels', 'default' => null],
								['module' => 'ups', 'method' => 'ups.', 'title' => 'United Parcel Service', 'default' => null],
								['module' => 'usps', 'method' => 'usps.', 'title' => 'United States Postal Service', 'default' => null],
								['module' => 'weight', 'method' => 'weight.weight_', 'title' => 'Weight shipping', 'default' => null],
								['module' => 'post24lithuania', 'method' => 'post24lithuania.post24lithuania', 'title' => 'Omniva', 'regex' => '/(\d+)\D*$/', 'default' => null],
								['module' => 'balticodedpdcourier', 'method' => 'balticodedpdcourier.balticodedpdcourier_', 'title' => 'DPD Courier', 'default' => null],
								['module' => 'balticodedpdparcelstore', 'method' => 'balticodedpdparcelstore.balticodedpdparcelstore', 'title' => 'DPD Siuntų taškai', 'regex' => '/(\d+)\D*$/', 'default' => null]
							);
	private $mimeTypes = array(
							'pdf' => 'application/pdf',
							'txt' => 'text/plain',
							'html' => 'text/html',
							'exe' => 'application/octet-stream',
							'zip' => 'application/zip',
							'doc' => 'application/msword',
							'xls' => 'application/vnd.ms-excel',
							'ppt' => 'application/vnd.ms-powerpoint',
							'gif' => 'image/gif',
							'png' => 'image/png',
							'jpeg' => 'image/jpg',
							'jpg' => 'image/jpg',
							'php' => 'text/plain'
						);
	
	public function index() {   
		$this->load->language('module/multisiuntos');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('multisiuntos', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_ms_success');
						
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
		$data['entry_courier_id'] = $this->language->get('entry_courier_id');
		$data['entry_express'] = $this->language->get('entry_express');
		$data['entry_pickup_required'] = $this->language->get('entry_pickup_required');
		$data['entry_loading_required'] = $this->language->get('entry_loading_required');
		$data['entry_saturday'] = $this->language->get('entry_saturday');
		$data['entry_insurance'] = $this->language->get('entry_insurance');
		$data['entry_proof'] = $this->language->get('entry_proof');
		$data['entry_status'] = $this->language->get('entry_ms_status');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_module_add'] = $this->language->get('button_module_add');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		//'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		//'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/multisiuntos', 'token=' . $this->session->data['token'], 'SSL'),
      		//'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('module/multisiuntos', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
				
		$data['modules'] = array();
		
		
		
		$this->load->model('localisation/geo_zone');

		$geo_zones = $this->model_localisation_geo_zone->getGeoZones();

		foreach ($geo_zones as $geo_zone) {
			if (isset($this->request->post['multisiuntos_' . $geo_zone['geo_zone_id'] . '_rate'])) {
				$data['multisiuntos_' . $geo_zone['geo_zone_id'] . '_rate'] = $this->request->post['multisiuntos_' . $geo_zone['geo_zone_id'] . '_rate'];
			} else {
				$data['multisiuntos_' . $geo_zone['geo_zone_id'] . '_rate'] = $this->config->get('multisiuntos_' . $geo_zone['geo_zone_id'] . '_rate');
			}		

			if (isset($this->request->post['multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'])) {
				$data['multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'] = $this->request->post['multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'];
			} else {
				$data['multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'] = $this->config->get('multisiuntos_' . $geo_zone['geo_zone_id'] . '_status');
			}		
		}

		$data['geo_zones'] = $geo_zones;
		
		if (isset($this->request->post['multisiuntos_status'])) {
			$data['multisiuntos_status'] = $this->request->post['multisiuntos_status'];
		} else {
			$data['multisiuntos_status'] = $this->config->get('multisiuntos_status');
		}
		
		if (isset($this->request->post['multisiuntos_express'])) {
			$data['multisiuntos_express'] = $this->request->post['multisiuntos_express'];
		} else {
			$data['multisiuntos_express'] = $this->config->get('multisiuntos_express');
		}
		
		if (isset($this->request->post['multisiuntos_pickup_required'])) {
			$data['multisiuntos_pickup_required'] = $this->request->post['multisiuntos_pickup_required'];
		} else {
			$data['multisiuntos_pickup_required'] = $this->config->get('multisiuntos_pickup_required');
		}
		
		if (isset($this->request->post['multisiuntos_loading_required'])) {
			$data['multisiuntos_loading_required'] = $this->request->post['multisiuntos_loading_required'];
		} else {
			$data['multisiuntos_loading_required'] = $this->config->get('multisiuntos_loading_required');
		}
		
		if (isset($this->request->post['multisiuntos_saturday'])) {
			$data['multisiuntos_saturday'] = $this->request->post['multisiuntos_saturday'];
		} else {
			$data['multisiuntos_saturday'] = $this->config->get('multisiuntos_saturday');
		}
		
		if (isset($this->request->post['multisiuntos_insurance'])) {
			$data['multisiuntos_insurance'] = $this->request->post['multisiuntos_insurance'];
		} else {
			$data['multisiuntos_insurance'] = $this->config->get('multisiuntos_insurance');
		}
		
		if (isset($this->request->post['multisiuntos_proof'])) {
			$data['multisiuntos_proof'] = $this->request->post['multisiuntos_proof'];
		} else {
			$data['multisiuntos_proof'] = $this->config->get('multisiuntos_proof');
		}
		
		if (isset($this->request->post['multisiuntos_module'])) {
			$data['modules'] = $this->request->post['multisiuntos_module'];
		} elseif ($this->config->get('multisiuntos_module')) { 
			$data['modules'] = $this->config->get('multisiuntos_module');
		}
		
		$show_shipping_list = array();
		$this->load->model('extension/extension');
		
		$results = $this->model_extension_extension->getInstalled('shipping');
		//print_r($results);
		foreach ($results as $result) {
			//echo "<br/>".$result."|||".$this->config->get($result . '_status')."</br>";
			if ($this->config->get($result . '_status')) {
				foreach ($this->shipping_list as $shipping_item)
				{
					if ($shipping_item['title']&&$shipping_item['module']==$result)
					{
						$show_shipping_list[] = array('name' => $shipping_item['title'], 'shipping_method' => $shipping_item['method']);
					}
				}
			}
		}
		//print_r($show_shipping_list);die;
		$data['shipping_methods'] = $show_shipping_list;
		
		$data['courier_ids'] = $this->show_courier_list;
		
		
		/*$this->template = 'module/multisiuntos.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());*/
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
	
		$this->response->setOutput($this->load->view('module/multisiuntos.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/multisiuntos')) {
			$this->error['warning'] = $this->language->get('error_ms_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function getShippingData($order){
		$shipping_data = array();
		$shipping_data['courier_id'] = '';
		$shipping_data['terminal'] = '';
		$shipping_data['cash_on_delivery'] = array();
		$shipping_data['express'] = false;
		$shipping_data['pickup_required'] = false;
		$shipping_data['loading_required'] = false;
		$shipping_data['saturday'] = false;
		$shipping_data['insurance'] = false;
		$shipping_data['proof'] = false;
		
		$shipping_list_configs = $this->config->get('multisiuntos_module');
		foreach ($this->shipping_list as $shipping_list_item)
		{
			if (strpos($order['shipping_code'], $shipping_list_item['method']) !== false)
			{
				if($shipping_list_configs)
				{
					foreach ($shipping_list_configs as $shipping_list_config)
					{
						if ($shipping_list_config['shipping_method'] == $shipping_list_item['method'])
						{
							$shipping_data['courier_id'] = $shipping_list_config['courier_id'];
							if (isset($shipping_list_item['regex']))
							{
								preg_match($shipping_list_item['regex'],$order['shipping_code'], $match);
								$shipping_data['terminal'] = $match[0];
							}
							if(in_array($shipping_list_config['courier_id'], $this->express_available)){
								$shipping_data['express'] = $shipping_list_config['express'];
							}
							$shipping_data['pickup_required'] = $shipping_list_config['pickup_required'];
							$shipping_data['loading_required'] = $shipping_list_config['loading_required'];
							$shipping_data['saturday'] = $shipping_list_config['saturday'];
							$shipping_data['insurance'] = $shipping_list_config['insurance'];
							$shipping_data['proof'] = $shipping_list_config['proof'];
						}
					}
				}
				break;
			}
		}
		
		if(in_array($order['payment_code'], $this->cod_methods)){
			
			$shipping_data['cash_on_delivery'] = array (
													'value' => $order['total'],
													'reference' => $order['invoice_prefix'].$order['invoice_no'],
													'currency' => $order['currency_code']
													);
		}
		if (!$shipping_data['courier_id'])
		{
			$shipping_data['courier_id'] = $order['shipping_code'];
		}
		
		return $shipping_data;
	}
	
	private function getDecimal($number, $precision, $scale){
		return number_format($number,$scale,".","");
	}
	
	private function download($file_name, $content, $file_type = 'application/octet-stream')
    {
		if(in_array($file_type, array_keys($this->mimeTypes))) {
            $file_type = $this->mimeTypes[$file_type];
        }
        $cretedownload = new createdownload($file_name, $file_type);
        $cretedownload->render($content);
    }
	
	public function multisiuntos_xml(){
		if (!isset($_POST['selected'])) {
			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));
		}

		
		$this->load->model('sale/order');
		$this->load->model('catalog/product');
		$this->load->model('multisiuntos/array2xml');
		
		
		$orders_array = array(
		    '@attributes' => array(
		        'version' => '1'
		    ),
		    'shipment' => array(
		    )
		);
		foreach ($_POST['selected'] as $orderId) {
			
			$order_data = $this->model_sale_order->getOrder($orderId);

			if ($order_data) {
				$products=$this->model_sale_order->getOrderProducts($orderId); //Load products of this order
				$weight=0;
				$quantity=0;
				foreach ($products as $product) {
					$product_data=$this->model_catalog_product->getProduct($product['product_id']);
					$weight+=$this->weight->convert($product_data['weight']*$product['quantity'], $product_data['weight_class_id'], 1); // 1 is kg $this->config->get('config_weight_class_id')
					$quantity += $product['quantity'];
				}
				
				
				
				
				
				
				$shipping_data = $this->getShippingData($order_data);
				
	
				$orders_array['shipment'][] = array(
				"reference" => $order_data['order_id'], //Orderio Id
				"weight" => (float)$this->getDecimal($weight,8,3), //Svoris kilogramais
				"remark" => $order_data['comment'], //Papildoma informacija, kuri eina ant lipduko
				"additional_information" => null, //Papildoma informacija
				//"number_of_parcels" => (int)$quantity, //Pakuociu skaicius per uzsakyma
				"number_of_parcels" => 1,
				"courier_identifier" => $shipping_data['courier_id'], //Kurjerio identifikatorius
				"receiver" => array( //Gavejo info
						"name" => $order_data['shipping_firstname'] . ' ' . $order_data['shipping_lastname'], //Vardas
						"street" => $order_data['shipping_address_1'] . ' ' . $order_data['shipping_address_2'], //Gatve
						"postal_code" => $order_data['shipping_postcode'], //Pasto kodas
						"city" => $order_data['shipping_city'], //Miestas
						"phone" => $order_data['telephone'], //Telefono numeris
						"email" => $order_data['email'], //Elektroninio pasto adresas
						"parcel_terminal_identifier" => $shipping_data['terminal'], //Siuntu tasko ID
						"country_code" => $order_data['shipping_iso_code_2'] //Salies kodas
					),
				"services" => array( //Vezejo info
					"cash_on_delivery" => $shipping_data['cash_on_delivery'], //Gauname COD duomenis
					"express_delivery" => (bool)$shipping_data['express'], //Ar tai Express pristatymas?
					"self_service" => (bool)$shipping_data['pickup_required'], //Ar reikalingas pakrovimas
					"loading_service" => (bool)$shipping_data['loading_required'], //Pakrovimo servisas
					"saturday_delivery" => (bool)$shipping_data['saturday'], //Savaitgalinis vezimas
					"insurance" => (bool)$shipping_data['insurance'], //Draudimas
					"proof_of_delivery" => (bool)$shipping_data['proof'] //Pristatymo patvirinimas
					)
				);
			}
		}
		
		
		$xml = $this->model_multisiuntos_array2xml->createXML('shipments', $orders_array);
		$this->download("couriers.xml", $xml->saveXML(), 'xml');
		die;
		
		$log = array('status' => 'err');
		if ($log&&$log['status'] == 'err'){
			$warning = $this->language->get('text_multisiuntos_xml_failed');
			$warning = $log['errlog'];
			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'].'&warning='.$warning, 'SSL'));
		} else {
			$this->session->data['success'] = 'Multisiuntos XML is success';
			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));

		}
	}
}
?>