<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-latest" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
	  <?php /*
	  <div class="vtabs"><a href="#tab-general"><?php echo $tab_general; ?></a>
        <?php foreach ($geo_zones as $geo_zone) { ?>
        <a href="#tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></a>
        <?php } ?>
      </div>
      */ ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-multisiuntos" class="form-horizontal">
        <div id="tab-general" <?php /*class="vtabs-content" */ ?>>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10"><select name="multisiuntos_status" class="form-control">
                  <?php if ($multisiuntos_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
			</div>
          </div>
		  
		  <table id="module" class="table table-striped table-bordered table-hover">
			<thead>
			  <tr>
				<td class="text-left"><?php echo $entry_shipping_method; ?></td>
				<td class="text-left"><?php echo $entry_courier_id; ?></td>
				<td class="text-left"><?php echo $entry_express; ?></td>
				<td class="text-left"><?php echo $entry_pickup_required; ?></td>
				<td class="text-left"><?php echo $entry_loading_required; ?></td>
				<td class="text-left"><?php echo $entry_saturday; ?></td>
				<td class="text-left"><?php echo $entry_insurance; ?></td>
				<td class="text-left"><?php echo $entry_proof; ?></td>
				<td></td>
			  </tr>
			</thead>
			<?php $module_row = 0; ?>
			<?php foreach ($modules as $module) { ?>
			<tbody id="module-row<?php echo $module_row; ?>">
			  <tr>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][shipping_method]" class="form-control">
					<?php foreach ($shipping_methods as $shipping_method) { ?>
					<?php if ($shipping_method['shipping_method'] == $module['shipping_method']) { ?>
					<option value="<?php echo $shipping_method['shipping_method']; ?>" selected="selected"><?php echo $shipping_method['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $shipping_method['shipping_method']; ?>"><?php echo $shipping_method['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][courier_id]" class="form-control">
					<?php foreach ($courier_ids as $courier_id) { ?>
					<?php if ($courier_id['courier_id'] == $module['courier_id']) { ?>
					<option value="<?php echo $courier_id['courier_id']; ?>" selected="selected"><?php echo $courier_id['name']; ?></option>
					<?php } else { ?>
					<option value="<?php echo $courier_id['courier_id']; ?>"><?php echo $courier_id['name']; ?></option>
					<?php } ?>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][express]" class="form-control">
					<?php if ($module['express']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][pickup_required]" class="form-control">
					<?php if ($module['pickup_required']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][loading_required]" class="form-control">
					<?php if ($module['loading_required']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][saturday]" class="form-control">
					<?php if ($module['saturday']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][insurance]" class="form-control">
					<?php if ($module['insurance']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><select name="multisiuntos_module[<?php echo $module_row; ?>][proof]" class="form-control">
					<?php if ($module['proof']) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select></td>
				<td class="text-left"><button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
			  </tr>
			</tbody>
			<?php $module_row++; ?>
			<?php } ?>
			<tfoot>
			  <tr>
				<td colspan="8"></td>
				<td class="text-left"><button type="button" onclick="addModule();" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
			  </tr>
			</tfoot>
		  </table>
		  
		  
		  
        </div>
        <?php /*foreach ($geo_zones as $geo_zone) { ?>
        <div id="tab-geo-zone<?php echo $geo_zone['geo_zone_id']; ?>" class="vtabs-content">
          <table class="form">
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select name="multisiuntos_<?php echo $geo_zone['geo_zone_id']; ?>_status">
                  <?php if (${'multisiuntos_' . $geo_zone['geo_zone_id'] . '_status'}) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
        <?php }*/ ?>
      </form>
	  

	  
	  
    </div>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';	
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][shipping_method]" class="form-control">';
	<?php foreach ($shipping_methods as $shipping_method) { ?>
	html += '      <option value="<?php echo $shipping_method['shipping_method']; ?>"><?php echo $shipping_method['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][courier_id]" class="form-control">';
	<?php foreach ($courier_ids as $courier_id) { ?>
	html += '      <option value="<?php echo $courier_id['courier_id']; ?>"><?php echo $courier_id['name']; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][express]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][pickup_required]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][loading_required]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][saturday]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][insurance]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><select name="multisiuntos_module[' + module_row + '][proof]" class="form-control">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="text-left"><button type="button" onclick="$(\'#module-row' + module_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}

$('.vtabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>